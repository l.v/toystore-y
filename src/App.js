import React, {useState} from 'react';
import './App.css';

import ToyList from "./modules/ToyList";


function App() {
    // create our state
    const [toys, setToys] = useState([
        {name:"cheval à bascule", nb: 6},
        {name:"legos", nb: 7},
        {name:"puzzle", nb: 3},
        {name:"poupée", nb:5}
    ]);
    /*
    const ret = useState(myToyList);
    const toys = ret[0];
    const setToys = ret[1];
    */

    // declare a function
    // (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function)
    // (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/function (fr))
    function addRandomToy() {
        //make a copy of the current state
        const newToys = [...toys];

        //add a toy
        newToys.push({name:'jouet', nb:5});

        //set the state with our copied and edited array
        setToys(newToys);
    }

    // define an anonymous function, and store it in decrementNb
    // (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/function)
    // (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Op%C3%A9rateurs/L_op%C3%A9rateur_function (fr))
    const decrementNb = (id) => {
        const tabCopy = [...toys]
        tabCopy[id].nb -= 1;
        setToys(tabCopy); 
    }

    //pass toys from our state and decrementNb to the ToyList as props
    return (
        <div className="App">
            <ToyList toys={toys} decrementNb={decrementNb} />
            <button onClick={addRandomToy}>Add A Toy</button>
        </div>
    );
}

export default App;
